﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;

namespace Tester
{
    //Models
    //   The models included here are for convenience, for the 
    //   purpose of serializing the objects and passing them to
    //   the WebAPI.
    public class Product
    {
        public int Id { get; set; }

        public string ProductName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public string Category { get; set; }

        public string Manufacture { get; set; }

        public string Rating { get; set; }
    }
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public string ApiKey { get; set; }
    }

    public class Sales
    {

        public int Id { get; set; }

        public DateTime Date { get; set; }

        //public virtual List<Sales> ListofSales { get; set; }

        public decimal Amount { get; set; }

        public string Email { get; set; }
    }

        //WebAPI Methods
        public class WebAPI {
        public WebAPI() { }
        public void runTestSuite()
        {
            //Verify the login methods work

            //Products
            Products_Get();

            var
            prd = new Product();
            prd.ProductName = "The Division " + new Random().Next().ToString();  //Key value, must be unique
            prd.CreatedDate = DateTime.Now;
            prd.LastModifiedDate = DateTime.Now;
            prd.Price = 60.00M;
            prd.Quantity = 420;
            prd.Category = "Xbox One";
            prd.Manufacture = "Ubisoft";
            prd.Rating = "Mature";

            var rid = Products_Post(prd);  //DB returns new record id (rid), save it

            Products_Get(rid);  //Ask the DB for the record by record id (rid)

            prd.ProductName = "5 noisiviD ehT";
            Products_Update(rid, prd); //Pass to DB the record id (rid) of the record to update

            Products_Delete(rid);      //Now delete the record

            //Categories

            Categories_Get();

            var ctg = new Category();
            ctg.Name = "Xbox 720";

            var cid = Category_Post(ctg); //DB returns new record id (rid), save it

            Category_Get(cid); //Ask the DB for the record by record id (rid)

            ctg.Name = "Xbox 730";

            Category_Update(cid, ctg);

            Category_Delete(cid);


            //Manufacturer

            Manufacturers_Get();

            var mnf = new Manufacturer();
            mnf.Name = "TEST";

            var mid = Manufacturer_Post(mnf);

            Manufacturer_Get(mid);

            mnf.Name = "TEST1";

            Manufacturer_Update(mid, mnf);

            Manufacturer_Delete(mid);

            //User
            Users_Get();

            var usr = new User();
            usr.FirstName = "Test";  //Key value, must be unique
            usr.LastName = "Test1";
            usr.Email = "test@selu.edu";
            usr.Password = "TestTest";
            //usr.ApiKey = "idk";
            

            var uid = User_Post(usr);  //DB returns new record id (rid), save it

            User_Get(uid);  //Ask the DB for the record by record id (rid)

            usr.FirstName = "TestUpdateName";
            User_Update(uid, usr); //Pass to DB the record id (rid) of the record to update

            User_Delete(uid);      //Now delete the record


            //Sales
            Sales_Get();

            var
            sls = new Sales();
            sls.Date = DateTime.Now;  //Key value, must be unique
            sls.Amount = 23.00M;
            sls.Email = "sa383@383.com";
            

            var sid = Sales_Post(sls);  //DB returns new record id (rid), save it

            Sale_Get(sid);  //Ask the DB for the record by record id (rid)

            //prd.ProductName = "5 noisiviD ehT";
           // Products_Update(rid, prd); //Pass to DB the record id (rid) of the record to update

            Sales_Delete(sid);


        }

        private void Sales_Delete(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.DELETE);
            request.Resource = "SalesAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("DELETE-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }

        private int Sales_Post(Sales sls)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            //int id = 0;
            var request = new RestRequest(Method.POST);
            request.Resource = "SalesAPI/";
            //request.AddParameter("id", id, ParameterType.GetOrPost);
            request.AddParameter("Date", sls.Date, ParameterType.GetOrPost);
            request.AddParameter("Amount", sls.Amount, ParameterType.GetOrPost);
            request.AddParameter("Email", sls.Email, ParameterType.GetOrPost);
         
            // execute the request
            IRestResponse response = client.Execute(request);

            var deserial = new RestSharp.Deserializers.JsonDeserializer();
            var x = deserial.Deserialize<Product>(response);
            sls.Id = x.Id;

            Console.WriteLine("POST-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

            return sls.Id;
        }

        private void Sales_Get()
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "SalesAPI/";

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }

        private void Sale_Get(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "SalesAPI/";
            request.AddParameter("id", id, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }

        // User CRUD operations method
        private void Users_Get()
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "UsersAPI/";

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
        private int User_Post(User usr)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            //int id = 0;
            var request = new RestRequest(Method.POST);
            request.Resource = "UsersAPI/";
            //request.AddParameter("id", id, ParameterType.GetOrPost);
            request.AddParameter("FirstName", usr.FirstName, ParameterType.GetOrPost);
            request.AddParameter("LastName", usr.LastName, ParameterType.GetOrPost);
            request.AddParameter("Email", usr.Email, ParameterType.GetOrPost);
            request.AddParameter("Password", usr.Password, ParameterType.GetOrPost);
            //request.AddParameter("ApiKey", usr.ApiKey, ParameterType.GetOrPost);



            // execute the request
            IRestResponse response = client.Execute(request);

            var deserial = new RestSharp.Deserializers.JsonDeserializer();
            var x = deserial.Deserialize<User>(response);
            usr.Id = x.Id;

            Console.WriteLine("POST-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

            return usr.Id;
        }
        private void User_Get(int id)
        {


            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "UsersAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response");
            Console.WriteLine(response.Content);
            Console.WriteLine();

        }
        private void User_Update(int id, User usr)
        {
        
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.PUT);
            request.Resource = "UsersAPI/" + id;

  

            request.AddJsonBody(usr);

            // execute the request
            IRestResponse response = client.Execute(request);

            var content = response.Content; // raw content as string

            Console.WriteLine("UPDATE-Response");
            Console.WriteLine(response.Content);
        

        }
        private void User_Delete(int id)
        {


            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.DELETE);
            request.Resource = "UsersAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("DELETE-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }



        //Manufacturers CRUD operations methods

        private void Manufacturers_Get()
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "ManufacturersAPI/";

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

        }
        private int Manufacturer_Post(Manufacturer mnf)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            //int id = 0;
            var request = new RestRequest(Method.POST);
            request.Resource = "ManufacturersAPI/";
            //request.AddParameter("id", id, ParameterType.GetOrPost);
            request.AddParameter("Name", mnf.Name, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            var deserial = new RestSharp.Deserializers.JsonDeserializer();
            var x = deserial.Deserialize<Manufacturer>(response);
            mnf.Id = x.Id;

            Console.WriteLine("POST-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

            return mnf.Id;
        }
        private void Manufacturer_Get(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "ManufacturersAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
        private void Manufacturer_Update(int id, Manufacturer mnf)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.PUT);
            request.Resource = "ManufacturersAPI/" + id;

            //var p = new Product();
            //p.Id = 50;
            //p.ProductName = "Boo";
            //p.LastModifiedDate = DateTime.Now;
            //p.CreatedDate = DateTime.Now;

            request.AddJsonBody(mnf);

            // execute the request
            IRestResponse response = client.Execute(request);

            var content = response.Content; // raw content as string

            Console.WriteLine("UPDATE-Response");
            Console.WriteLine(response.Content);
        }
        private void Manufacturer_Delete(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.DELETE);
            request.Resource = "ManufacturersAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("DELETE-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        
    }







        //Categories CRUD operations methods
        private void Categories_Get()
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "CategoriesAPI/";

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

        }
        private int Category_Post(Category ctg)
        {

            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            //int id = 0;
            var request = new RestRequest(Method.POST);
            request.Resource = "CategoriesAPI/";
            //request.AddParameter("id", id, ParameterType.GetOrPost);
            request.AddParameter("Name", ctg.Name, ParameterType.GetOrPost);



            // execute the request
            IRestResponse response = client.Execute(request);

            var deserial = new RestSharp.Deserializers.JsonDeserializer();
            var x = deserial.Deserialize<Category>(response);
            ctg.Id = x.Id;

            Console.WriteLine("POST-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

            return ctg.Id;
        }
        private void Category_Get(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "CategoriesAPI/";
            request.AddParameter("Id", id, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
        private void Category_Update(int id, Category ctg)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.PUT);
            request.Resource = "CategoriesAPI/" + id;

           
            request.AddJsonBody(ctg);

            // execute the request
            IRestResponse response = client.Execute(request);

            var content = response.Content; // raw content as string

            Console.WriteLine("UPDATE-Response");
            Console.WriteLine(response.Content);
        }
        private void Category_Delete(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.DELETE);
            request.Resource = "CategoriesAPI/";
            request.AddParameter("id", id, ParameterType.GetOrPost);

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("DELETE-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }





        //Products CRUD operations methods

        private void Products_Get()
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "ProductsAPI/";

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
        private int Products_Post(Product prd)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            //int id = 0;
            var request = new RestRequest(Method.POST);
            request.Resource = "ProductsAPI/";
            //request.AddParameter("id", id, ParameterType.GetOrPost);
            request.AddParameter("ProductName", prd.ProductName, ParameterType.GetOrPost);
            request.AddParameter("CreatedDate", prd.CreatedDate, ParameterType.GetOrPost);
            request.AddParameter("LastModifiedDate", prd.LastModifiedDate, ParameterType.GetOrPost);
            request.AddParameter("Price", prd.Price, ParameterType.GetOrPost);
            request.AddParameter("Quantity", prd.Quantity, ParameterType.GetOrPost);
            request.AddParameter("Category", prd.Category, ParameterType.GetOrPost);
            request.AddParameter("Manufacture", prd.Manufacture, ParameterType.GetOrPost);
            request.AddParameter("Rating", prd.Rating, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            var deserial = new RestSharp.Deserializers.JsonDeserializer();
            var x = deserial.Deserialize<Product>(response);
            prd.Id = x.Id;

            Console.WriteLine("POST-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();

            return prd.Id;
        }
        private void Products_Get(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.GET);
            request.Resource = "ProductsAPI/";
            request.AddParameter("id", id, ParameterType.GetOrPost);


            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("GET-Response");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
        private void Products_Update(int id, Product prd)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.PUT);
            request.Resource = "ProductsAPI/" + id;

            //var p = new Product();
            //p.Id = 50;
            //p.ProductName = "Boo";
            //p.LastModifiedDate = DateTime.Now;
            //p.CreatedDate = DateTime.Now;

            request.AddJsonBody(prd);

            // execute the request
            IRestResponse response = client.Execute(request);

            var content = response.Content; // raw content as string

            Console.WriteLine("UPDATE-Response");
            Console.WriteLine(response.Content);
        }
        private void Products_Delete(int id)
        {
            var client = new RestClient("http://localhost:51218/");
            client.Authenticator = new HttpBasicAuthenticator("Admin", "Password");

            var request = new RestRequest(Method.DELETE);
            request.Resource = "ProductsAPI/";
            request.AddParameter("id", id, ParameterType.GetOrPost);

            // execute the request
            IRestResponse response = client.Execute(request);

            Console.WriteLine("DELETE-Response:");
            Console.WriteLine(response.Content);
            Console.WriteLine();
        }
    }
    



}

