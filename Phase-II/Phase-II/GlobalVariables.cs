﻿using System;

namespace PhaseII
{
	public class GlobalVariables
	{
		public string userAPIkey { get; set; }
		public int userID { get; set; }
	}
}