﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class EditManufacturePage : ContentPage
	{
		public EditManufacturePage ()
		{

			Entry manufacture;

			Button button;


			Title = "Edit Page";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(manufacture = new Entry { Placeholder = "Manufacture" }),
					(button = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") }),
				}
			};
			button.Clicked += (sender, e) => {
				App.Current.MainPage = new TabPage();

			};
		}
	}
}


