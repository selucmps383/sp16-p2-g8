﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class AddProductPage : ContentPage
	{
		public AddProductPage ()
		{

			Entry productName;
			Entry category;
			Entry manufacture;
			Entry price;
			Entry quantity;
			Entry rating;

			Button button;


			Title = "Add a Product Page";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(productName = new Entry { Placeholder = "Product Name" }),
					(category = new Entry { Placeholder = "Category" }),
					(manufacture = new Entry { Placeholder = "Manufacture" }),
					(price = new Entry { Placeholder = "Price" }),
					(quantity = new Entry { Placeholder = "Quantity" }),
					(rating = new Entry { Placeholder = "Rating" }),
					(button = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") }),
				}
			};
			button.Clicked += (sender, e) => {
				App.Current.MainPage = new TabPage();

			};
		}
	}
}


