﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class EditProductPage : ContentPage
	{
		public EditProductPage ()
		{
			Entry productName;
			Entry category;
			Entry manufacture;
			Entry price;
			Entry quantity;
			Entry rating;

			Button button;


			Title = "Edit Page";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(productName = new Entry { Placeholder = "Product Name" }),
					(category = new Entry { Placeholder = "Category" }),
					(manufacture = new Entry { Placeholder = "Manufacture" }),
					(price = new Entry { Placeholder = "Price" }),
					(quantity = new Entry { Placeholder = "Quantity" }),
					(rating = new Entry { Placeholder = "Rating" }),
					(button = new Button { Text = "Edit", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") }),
				}
			};
			button.Clicked += (sender, e) => {
				App.Current.MainPage = new TabPage();

			};
		}
	}
}


