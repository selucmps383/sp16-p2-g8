﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class EditCategoryPage : ContentPage
	{
		public EditCategoryPage ()
		{

			Entry category;

			Button button;


			Title = "Edit Page";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(category = new Entry { Placeholder = "Category" }),
					(button = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") }),
				}
			};
			button.Clicked += (sender, e) => {
				App.Current.MainPage = new TabPage();

			};
		}
	}
}


