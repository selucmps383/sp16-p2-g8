﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class ManufacturesPage : ContentPage
	{
		Button addButton;

		public ManufacturesPage ()
		{

			Title = "Manufactures";
			Content = new StackLayout {
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(addButton = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex("add8e6") }),
					new TableView { 
						Intent = TableIntent.Form,
						Root = new TableRoot () { 
							new TableSection ("Manufactures") {
								new KeyValueCell ("343 Industries", ""),
								new KeyValueCell ("Bandai", ""),
								new KeyValueCell ("Bethesda", ""),
								new KeyValueCell ("Bungie", ""),
							}
						}
					}
				}
			};
			addButton.Clicked += (sender, e) => {
				App.Current.MainPage = new EditManufacturePage();
			};
		}
	}
}


