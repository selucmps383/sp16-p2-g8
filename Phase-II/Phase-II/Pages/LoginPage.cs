﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable.Serializers;
using System.Threading.Tasks;
using System.Net;
using RestSharp.Portable;

namespace PhaseII
{
	public class LoginPage : ContentPage
	{
		Entry email;
		Entry password;
		Button button;

		private const string baseURL = "http://10.71.34.1:51218/";
		private string userAPIkey = "";
		public int userId = -1; 


		public LoginPage ()
		{

			Title = "Login Page";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(email = new Entry { Placeholder = "Email" }),
					(password = new Entry { Placeholder = "Password", IsPassword = true }),
					(button = new Button { Text = "Login", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") }),
				}
			};
			button.Clicked += async (sender, e) => {
//				string url = "api/ApiKey?email=" + email.Text + "&password=" + password.Text;
//				var json = await FetchApiKeyAndUserId(url);
//
//				userAPIkey = json.ApiKey;
//				userId = json.Id;
//
//				if(userId != -1 && string.IsNullOrEmpty(userAPIkey))
//				{
					App.Current.MainPage = new TabPage();
//				}
//				else
//				{
//					DisplayAlert("Invalid Login", "Either the username or password is incorrect.", "Try again");
//				}

			};

		}

		public async Task<UserModel> FetchApiKeyAndUserId(string url)
		{
			RestClient client = new RestClient (baseURL);
			var request = new RestSharp.Portable.RestRequest (baseURL + url, Method.GET);
			var response = await client.Execute<UserModel>(request);
			return response.Data;
		}
	}
}


