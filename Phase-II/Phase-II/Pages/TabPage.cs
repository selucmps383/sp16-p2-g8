﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class TabPage : TabbedPage
	{
		public TabPage ()
		{
			this.Children.Add (new ProductsPage ());
			this.Children.Add (new CategoriesPage ());
			this.Children.Add (new ManufacturesPage ());
			this.Children.Add (new LogOutPage ());
		}
	}
}


