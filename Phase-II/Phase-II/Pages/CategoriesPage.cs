﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class CategoriesPage : ContentPage
	{
		Button addButton;

		public CategoriesPage ()
		{

			Title = "Categories";

			Content = new StackLayout {
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(addButton = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex ("add8e6") }),
					new TableView { 
						Intent = TableIntent.Form,
						Root = new TableRoot () { 
							new TableSection ("Categories") {
								new KeyValueCell ("PS4", ""),
								new KeyValueCell ("PC", ""), 
								new KeyValueCell ("Xbox One", ""), 
								new KeyValueCell ("Wii U", "")
							}
						}
					}
				}
			};
			addButton.Clicked += (sender, e) => {
				App.Current.MainPage = new EditManufacturePage();
			};
		}
	}
}


