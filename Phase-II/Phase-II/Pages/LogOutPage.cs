﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class LogOutPage : ContentPage
	{
		Button button;

		public LogOutPage ()
		{

			Title = "LogOut";

			Content = new StackLayout {
				Spacing = 20,
				Padding = 50,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(button = new Button { Text = "Log Out", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") })
				}
			};
			button.Clicked += (sender, e) => {
				App.Current.MainPage = new LoginPage();

			};

		}
	}
}


