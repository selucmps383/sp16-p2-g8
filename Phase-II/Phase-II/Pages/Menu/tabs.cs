﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class tabs
	{
		public string Name {
			get;
			set;
		}

		public Func<ContentPage> PageFn {
			get;
			set;
		}

		public tabs (string name, Func<ContentPage> pageFn)
		{
			Name = name;
			PageFn = pageFn;
		}
	}
}


