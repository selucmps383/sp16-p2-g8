﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace PhaseII
{
	public class MenuPage : ContentPage
	{
		public Action<ContentPage> OnMenuSelect {
			get;
			set;
		}

		public MenuPage ()
		{
			Title = "Menu";

			Padding = new Thickness (10, 20);

			var tabs = new List<tabs> () {
				new tabs("Users", () => new LoginPage()),
				new tabs("Products", () => new LoginPage()),
				new tabs("Categories", () => new LoginPage()),
				new tabs("Manufactures", () => new LoginPage())
			};

			var dataTemplate = new DataTemplate (typeof(TextCell));
			dataTemplate.SetBinding (TextCell.TextProperty, "Name");

			var listView = new ListView () {
				ItemsSource = tabs,
				ItemTemplate = dataTemplate
			};

//			listView.ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
//				if (OnMenuSelect != null)
//				{
//					var tab = (String) e.SelectedItem;
//					var categoryPage = tab.PageFn();
//					OnMenuSelect(categoryPage);
//				}				
//			};


			Content = listView;
		}
	}
}


