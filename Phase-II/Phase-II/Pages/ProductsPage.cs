﻿using System;
using XLabs.Forms.Controls;
using Xamarin.Forms;

namespace PhaseII
{
	public class ProductsPage : ContentPage
	{
		Button button;
		Button button2;
		Button editButton;
		Button addButton;
		Button extraButton= new Button { Text = "Edit", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065")};
		PopupLayout _PopUpLayout = new PopupLayout();


		String productName = "Halo 5: Guardians";
		String category = "Xbox One";
		String manufacture = "343 Industries";
		decimal price = 37.99M;
		int quantity = 117;
		String rating = "Teen";

		public ProductsPage ()
		{
			var popup = new PopupLayout {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						(editButton = new Button { Text = "Edit", TextColor = Color.White, BackgroundColor = Color.FromHex ("77D065") })
					}
				}
			};

			Title = "Products";

			var layout = new StackLayout {
				VerticalOptions = LayoutOptions.Center,
				Children = {
					(addButton = new Button { Text = "Add New", TextColor = Color.White, BackgroundColor = Color.FromHex("add8e6") }),
					new TableView { 
						Intent = TableIntent.Form,
						Root = new TableRoot () { 

							new TableSection (productName) {						//Product Name
								new KeyValueCell ("Category", category),
								new KeyValueCell ("Create Date", DateTime.Now.ToString ()),
								new KeyValueCell ("Last Modified Date", DateTime.Now.ToString ()),
								new KeyValueCell ("Manufacture", manufacture),
								new KeyValueCell ("Price", price.ToString()),
								new KeyValueCell ("Quantity", quantity.ToString()),
								new KeyValueCell ("Rating", rating),
							}
						}
					},
					(button2 = new Button { Text = "Edit", TextColor = Color.White,
						BackgroundColor = Color.FromHex ("77D065")}),

					new TableView { 
						Intent = TableIntent.Form,
						Root = new TableRoot () {
							new TableSection ("The Order: 1886") {							//Product Name
								new KeyValueCell ("Category", "PS4"),
								new KeyValueCell ("Create Date", DateTime.Now.ToString ()),
								new KeyValueCell ("Last Modified Date", DateTime.Now.ToString ()),
								new KeyValueCell ("Manufacture", "Ready at Dawn"),
								new KeyValueCell ("Price", "26.50"),
								new KeyValueCell ("Quantity", "1886"),
								new KeyValueCell ("Rating", "Mature"),
							}
						}
					},
					(button = new Button { Text = "Edit", TextColor = Color.White,
						BackgroundColor = Color.FromHex ("77D065")}),
				}
			};

			TableView extraTable = new TableView { 
				Intent = TableIntent.Form,
				Root = new TableRoot () {
					new TableSection ("[Xbox One] Destiny: The Taken King") {							//Product Name
						new KeyValueCell ("Category", "Xbox One"),
						new KeyValueCell ("Create Date", DateTime.Now.ToString ()),
						new KeyValueCell ("Last Modified Date", DateTime.Now.ToString ()),
						new KeyValueCell ("Manufacture", "Bungie"),
						new KeyValueCell ("Price", "39.00"),
						new KeyValueCell ("Quantity", "7"),
						new KeyValueCell ("Rating", "Teen"),
					}
				}
			};

			layout.Children.Add (extraTable);
			layout.Children.Add (extraButton);

			var scrollView = new ScrollView { Content = layout };

			Content = scrollView;

			button.Clicked += (sender, e) => {
				//_PopUpLayout.ShowPopup(popup);
				App.Current.MainPage = new EditProductPage();
			};

			button2.Clicked += (sender, e) => {
				//_PopUpLayout.ShowPopup(popup);
				App.Current.MainPage = new EditProductPage();
			};

			extraButton.Clicked += (sender, e) => {
				//_PopUpLayout.ShowPopup(popup);
				App.Current.MainPage = new EditProductPage();
			};

			addButton.Clicked += (sender, e) => {
				App.Current.MainPage = new AddProductPage();
			};
		}
	}
}