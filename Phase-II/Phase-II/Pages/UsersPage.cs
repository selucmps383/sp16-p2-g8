﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class UsersPage : ContentPage
	{
		public UsersPage ()
		{

			Title = "Users";

			Content = new TableView { 
				Intent = TableIntent.Form,
				Root = new TableRoot () { 
					new TableSection ("sa@383.com") {					//User Email
						new KeyValueCell ("First Name", "Admin"),		//First Name
						new KeyValueCell ("Last Name", "Admin"),		//Last Name
						new KeyValueCell ("Password", "password"),		//Pasword
					}
				}
			};
		}
	}
}


