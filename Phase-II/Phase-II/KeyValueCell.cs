﻿using System;
using Xamarin.Forms;

namespace PhaseII
{
	public class KeyValueCell : ViewCell
	{
		public KeyValueCell (string key, string value)
		{
			View = new StackLayout () {
				Padding = new Thickness (15, 10),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.Center,
				Children = {
					new Label () { 
						Text = key,
						TextColor = Color.Green,
						HorizontalOptions = LayoutOptions.StartAndExpand
					},
					new Label () { 
						Text = value,
						TextColor = Color.White,
						HorizontalOptions = LayoutOptions.EndAndExpand
					}
				}
			};
		}
	}
}