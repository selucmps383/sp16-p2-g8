﻿using System;

using Xamarin.Forms;

namespace PhaseII
{
	public class MasterDetail : MasterDetailPage
	{
		public MasterDetail ()
		{
			var menuPage = new MenuPage ();
			menuPage.OnMenuSelect = (LoginPage) => {
				Detail = new NavigationPage(LoginPage);
				IsPresented = false;
			};

			Master = menuPage;

			Detail = new NavigationPage(new LoginPage ());

			MasterBehavior = MasterBehavior.Split;
		}
	}
}


