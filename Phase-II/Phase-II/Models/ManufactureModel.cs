﻿using System;

namespace PhaseII
{
	public class ManufactureModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}