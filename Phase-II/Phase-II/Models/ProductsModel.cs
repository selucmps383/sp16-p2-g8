﻿using System;

namespace PhaseII
{
	public class ProductsModel
	{
		public int Id { get; set; }
		public string ProductName { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime LastModifiedDate { get; set; }
		public decimal Price { get; set; }
		public int Quantity { get; set; }
		public string Category { get; set; }
		public string Manufacture { get; set; }
		public string Rating { get; set; }

		public int productsCount  { get; set; }
	}
}