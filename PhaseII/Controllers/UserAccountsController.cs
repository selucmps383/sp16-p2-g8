﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PhaseII.Models;
using System.Web.Helpers;
using System.Web.Security;

namespace PhaseII.Controllers
{
    //Require authentication for all pages (in this controller) unless otherwise specified
   
    public class UserAccountsController : Controller
    {
        private PhaseIIContext db = new PhaseIIContext();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(PhaseII.Models.User User)
        {

            if (IsValid(User.Email, User.Password))
            {
                User currentUser = db.Users.FirstOrDefault(u => u.Email == User.Email);
                if (currentUser != null) Session["CurrentUserId"] = currentUser.Id;

                FormsAuthentication.SetAuthCookie(User.Email, true);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Login information is incorrect.");
            return View(User);
        }

        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }

    }
}
