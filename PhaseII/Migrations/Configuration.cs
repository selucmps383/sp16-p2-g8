using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Helpers;
using Antlr.Runtime.Misc;
using PhaseII.Controllers;
using PhaseII.Models;
using System.Linq;


namespace PhaseII.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PhaseII.Models.PhaseIIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(PhaseII.Models.PhaseIIContext context)
        {
            //Seed User
            context.Users.AddOrUpdate(u => u.Email, new User() { FirstName = "Admin", LastName = "Admin", Email = "sa@383.com", Password = Crypto.HashPassword("password") });

            //Seed Categories
            context.Categories.AddOrUpdate(c => c.Name, new Category() { Name = "PS4" });
            context.Categories.AddOrUpdate(c => c.Name, new Category() { Name = "PC" });
            context.Categories.AddOrUpdate(c => c.Name, new Category() { Name = "Xbox One" });
            context.Categories.AddOrUpdate(c => c.Name, new Category() { Name = "Wii U" });
            context.Categories.AddOrUpdate(c => c.Name, new Category() { Name = "test" });

            //Seed Manufacturers
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "343 Industries" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Bandai" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Bethesda" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Bungie" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Capcom" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Hello Games" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Mojang" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Naughty Dog" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Nintendo" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Ready At Dawn" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Treyarch" });
            context.Manufacturers.AddOrUpdate(m => m.Name, new Manufacturer() { Name = "Ubisoft" });


            //Seed Products
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Halo 5: Guardians", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "343 Industries", Price = 37.99M, Quantity = 117, Rating = "Teen" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "The Order: 1886", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ready at Dawn", Price = 26.50M, Quantity = 1886, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Destiny: The Taken King", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bungie", Price = 35.77M, Quantity = 34, Rating = "Teen" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Destiny: The Taken King", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bungie", Price = 39.00M, Quantity = 37, Rating = "Teen" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "No Man's Sky", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Hello Games", Price = 59.99M, Quantity = 97, Rating = "Rating Pending" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Dark Souls III", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bandai", Price = 59.99M, Quantity = 48, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Fallout 4", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bethesda", Price = 39.88M, Quantity = 65, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Uncharted 4: A Thief's End", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Naughty Dog", Price = 59.88M, Quantity = 59, Rating = "Teen" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Dark Souls III", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bandai", Price = 59.99M, Quantity = 74, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Fallout 4", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bethesda", Price = 39.88M, Quantity = 43, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Call of Duty: Black Ops III", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Treyarch", Price = 39.99M, Quantity = 96, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Call of Duty: Black Ops III", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Treyarch", Price = 39.99M, Quantity = 112, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PC] Call of Duty: Black Ops III", Category = "PC", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Treyarch", Price = 25.27M, Quantity = 54, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Dead Rising 3: Apocalypse Edition", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Capcom", Price = 25.49M, Quantity = 117, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Minecraft: Xbox One Edition", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Mojang", Price = 17.45M, Quantity = 108, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Minecraft: PlayStation 4 Edition", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Mojang", Price = 18.84M, Quantity = 119, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Minecraft", Category = "PC", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Mojang", Price = 26.95M, Quantity = 117, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Far Cry Primal", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 59.49M, Quantity = 35, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PC] Far Cry Primal", Category = "PC", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 59.96M, Quantity = 65, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Far Cry Primal", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 59.88M, Quantity = 33, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PC] Fallout 4", Category = "PC", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bethesda", Price = 31.99M, Quantity = 23, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PC] Dark Souls III", Category = "PC", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Bandai", Price = 59.99M, Quantity = 87, Rating = "Mature" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Wii U] Just Dance 2015", Category = "Wii U", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 16.38M, Quantity = 116, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[Xbox One] Just Dance 2015", Category = "Xbox One", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 17.99M, Quantity = 50, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "[PS4] Just Dance 2015", Category = "PS4", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Ubisoft", Price = 17.43M, Quantity = 112, Rating = "Everyone 10+" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Donkey Kong Country Tropical Freeze", Category = "Wii U", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Nintendo", Price = 46.45M, Quantity = 109, Rating = "Everyone" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Mario Tennis: Ultra Smash", Category = "Wii U", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Nintendo", Price = 38.56M, Quantity = 80, Rating = "Everyone" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Hyrule Warriors", Category = "Wii U", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Nintendo", Price = 52.25M, Quantity = 109, Rating = "Teen" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "Kirby & The Rainbow Curse", Category = "Wii U", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "Nintendo", Price = 29.97M, Quantity = 121, Rating = "Everyone" });
            context.Products.AddOrUpdate(p => p.ProductName, new Product() { ProductName = "test", Category = "test", CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Manufacture = "test", Price = 29.97M, Quantity = 121, Rating = "Everyone" });

            //Seed Sales
            context.Sales.AddOrUpdate(s => s.Date, new Sales() { Date = DateTime.Now, Amount = 20.00M, Email = "sa383@383.com" });

        }
    }
}
