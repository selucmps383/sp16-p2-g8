namespace PhaseII.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductName = c.String(maxLength: 512, unicode: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastModifiedDate = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        Category = c.String(maxLength: 512, unicode: false),
                        Manufacture = c.String(maxLength: 512, unicode: false),
                        Rating = c.String(maxLength: 15, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProductName, unique: true);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        ApiKey = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "ProductName" });
            DropTable("dbo.Users");
            DropTable("dbo.Products");
        }
    }
}
