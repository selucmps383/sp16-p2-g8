﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class Manufacturer
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("Manufacturer")]
        [StringLength(35, ErrorMessage = "Manufacturer name cannot be longer than 35 characters.")]
        public string Name { get; set; }

        public virtual List<Manufacturer> ListofManufacturer { get; set; }
    }
}