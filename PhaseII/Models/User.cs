﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        
        public int Id { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("First Name")]
        [StringLength(35, ErrorMessage = "First Name cannot be longer than 35 characters.")]
        public string FirstName { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("Last Name")]
        [StringLength(35, ErrorMessage = "Last Name cannot be longer than 35 characters.")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid Email Address.")]
        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [DisplayNameAttribute("Email")]
        [StringLength(512, ErrorMessage = "Your Email cannot be longer than 512 characters.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("Password")]
        [MinLength(8, ErrorMessage = "Your password needs to be at least 8 characters.")]
        [StringLength(512, ErrorMessage = "Your password cannot be longer than 512 characters.")]
        public string Password { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("ApiKey")]
        public string ApiKey { get; set; }
    }
}