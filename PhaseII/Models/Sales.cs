﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class Sales
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "datetime")]
        [DisplayName("Sale Date")]
        public DateTime Date { get; set; }

        public virtual List<Sales> ListofSales { get; set; }

        [Range(0, Int32.MaxValue)]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("Email")]
        public string Email { get; set; }
    }
}