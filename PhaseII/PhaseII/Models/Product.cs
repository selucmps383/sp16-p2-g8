﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class Product
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [DisplayName("Product Name")]
        [StringLength(512, ErrorMessage = "Product Name cannot be longer than 35 characters.")]
        public string ProductName { get; set; }

        [Column(TypeName = "datetime")]
        [DisplayNameAttribute("Created Date")]
        public DateTime CreatedDate { get; set; }

        [Column(TypeName = "datetime")]
        [DisplayNameAttribute("Last Modified Date")]
        public DateTime LastModifiedDate { get; set; }

        [Range(0, Int32.MaxValue)]
        [DataType(DataType.Currency)]
        [DisplayNameAttribute("Price")]
        public decimal Price { get; set; }

        [Range(0, Int32.MaxValue)]
        [DisplayNameAttribute("Quantity")]
        public int Quantity { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("Category")]
        [StringLength(512, ErrorMessage = "First Name cannot be longer than 512 characters.")]
        public string Category { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayNameAttribute("Manufacture")]
        [StringLength(512, ErrorMessage = "First Name cannot be longer than 512 characters.")]
        public string Manufacture { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("ESRP Rating")]
        [StringLength(15, ErrorMessage = "Product Name cannot be longer than 15 characters.")]
        public string Rating { get; set; }
    }
}