﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class PhaseIIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public PhaseIIContext() : base("name=PhaseIIContext")
        {
        }

        public System.Data.Entity.DbSet<PhaseII.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<PhaseII.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<PhaseII.Models.Manufacturer> Manufacturers { get; set; }

        public System.Data.Entity.DbSet<PhaseII.Models.Category> Categories { get; set; }
    }
}
