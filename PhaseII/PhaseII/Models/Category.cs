﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhaseII.Models
{
    public class Category
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("Category")]
        [StringLength(35, ErrorMessage = "Category name cannot be longer than 35 characters.")]
        public string Name { get; set; }

        public virtual List<Category> ListofCategories { get; set; }
    }
}