﻿using PhaseII.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using System.Web.Helpers;
using System.Data.Entity.Migrations;
using System.Web.Security;
using System.Web.Http.Description;

namespace PhaseII.Controllers
{
    public class ApiKeyController : ApiController
    {
        public PhaseIIContext db = new PhaseIIContext();

        [HttpGet]
        public IHttpActionResult GetApiKey(string email, string password)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {

                var bytes = new byte[16];
                rng.GetBytes(bytes);
                if (IsValid(email, password)){
                    var curruser = db.Users.FirstOrDefault(u => u.Email == email);
                    curruser.ApiKey = Convert.ToBase64String(bytes);
                    db.Users.AddOrUpdate(curruser);
                    db.SaveChanges();
                    FormsAuthentication.SetAuthCookie(email, true);
                    return Ok(curruser.Id+Convert.ToBase64String(bytes));
                }

            }
            return Ok();
        }

        //public HttpResponseMessage GetUser([FromUri] User user)
        //{

        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);

        //    if (IsValid(user.Email, user.Password))
        //    {
        //        var stud = db.Users.FirstOrDefault(u => u.Email.Equals(user.Email));
        //        var apikey = GetApiKey();
        //        stud.ApiKey = apikey;
        //        db.Users.AddOrUpdate(stud);
        //        db.SaveChanges();


        //        response = Request.CreateResponse(HttpStatusCode.OK, stud);
        //    }

        //    return response;
        //}

        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Email.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }
    }
}